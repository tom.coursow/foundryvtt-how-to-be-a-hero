export const htbh = {};

htbh.rarityTypes = {
    junk: "htbh.rarityType.junk",
    normal: "htbh.rarityType.normal",
    rare: "htbh.rarityType.rare",
    exotic: "htbh.rarityType.exotic",
    legendary: "htbh.rarityType.legendary"
}