export default class ItemSheetHTBH extends ItemSheet {
    get template() {
        return `systems/howtobeahero/templates/items/${this.item.data.type}.html`;
    }


    getData(options) {
        const data = super.getData(options);
        data.config = CONFIG.htbh;
        return data;
    }
}