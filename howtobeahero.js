import {htbh} from "./module/config.js";
import ItemSheetHTBH from "./module/item/sheet.js";

Hooks.once("init", function () {
    console.log("htbh | Initialising How to be a Hero System...");

    CONFIG.htbh = htbh;

    // Unregister Foundry Core Sheets
    Items.unregisterSheet("core", ItemSheet);
    // Register custom How to be a Hero Sheets
    Items.registerSheet("howtobeahero", ItemSheetHTBH, {makeDefault: true});
})